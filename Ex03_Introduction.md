Introduction to `ORCA`
======================

[`ORCA`](https://www.faccts.de/orca/) is the multipurpose quantum chemical software package that we will be using during this course.
`ORCA` is being developed in the department of molecular theory and spectroscopy at the Max-Planck institute for coal research under the leadership of Prof. Dr. Frank Neese. It is free to use for academic purposes and can be downloaded (after [registration](https://orcaforum.kofo.mpg.de/ucp.php?mode=register)) on the [official forum](https://orcaforum.kofo.mpg.de/app.php/dlext/).


I. Installing the Software
--------------------------

### `ORCA`

Before you can download `ORCA`, you first need to [register yourself](https://orcaforum.kofo.mpg.de/ucp.php?mode=register) on the official forum.

You can then [download `ORCA 5.0.2`](https://orcaforum.kofo.mpg.de/app.php/dlext/?cat=18) for your respective operating system.  
(We choose version `5.0.2` as it is the most up-to-date at the time of writing. Other version might work as well.)

Once the files are downloaded, install `ORCA` to your machine and add the path to its binaries to your `$PATH` ([what?](https://www.orcasoftware.de/tutorials_orca/first_steps/trouble_install.html#assigning-path-variables)).

Here are some additional resources for the `ORCA` software package:

- [The Tutorials](https://www.orcasoftware.de/tutorials_orca/index.html)
- [The Input Library](https://sites.google.com/site/orcainputlibrary/home)
- [The Manual](https://orcaforum.kofo.mpg.de/app.php/dlext/?view=detail&df_id=165)
- [The User Forum](https://orcaforum.kofo.mpg.de/viewforum.php?f=8)

### Visualization Software

To build molecules and to inspect calculation results, several visualization programs are available online. Feel free to choose whichever one you prefer. Here are some recommendations:

- [Avogadro](https://avogadro.cc/) (free)
- [Chemcraft](https://www.chemcraftprog.com/) (free 180 day trial)
- [Jmol](http://jmol.sourceforge.net/) (free)

This exercise is made with Avogadro.


II. Using the Software
----------------------

A typical quantum chemical investigation begins with the identification of a stable structure of the compound of interest. Starting from a Lewis structure, the 3-dimensional molecule is constructed ('Draw Tool' in Avogadro). The crude initial structure is then pre-optimized using an empirical force field ('Auto Optimization Tool' in Avogadro). This gives a good initial geometry to start a more sophisticated geometry optimization with `ORCA` to get the final geometry.

![Acetic Acid Structure in Avogadro](/source/images/ex_3/acetic_acid_avogadro.png) *Unoptimized (left) and preoptimized (right) structures of acetic acid in Avogadro.*

In this exercise, you will exercise such a workflow, by taking a closer look at **Aromatic Compounds**.  
The characteristic aroma of certain foods can be attributed to a select few 'character impact' compounds.  

### 1. Create an Initial Geometry

---

#### Task 1
Choose two aroma compounds from Figure 1 and build their 3-dimensional geometries in Avogadro.

---

![Aromatic compounds and their foods.](/source/images/ex_3/character_impact_compounds.png) *Some character impact compounds and their associated foods.*

Make sure to pre-optimize your geometries with the 'Auto Optimization Tool' and then save each initial geometry as a `.xyz` file.

*Hints:*  
- Use keyboard shortcuts (`1` for single, `2` for double bonds, `O` for oxygen, `C` for carbon, ..)  
- Focus on the atom connectivity first, then use the 'Auto Optimization Tool' for plausible bond lengths and angles  
- You can manually move atoms during the 'Auto Optimization' to ensure the correct stereochemistry

### 2. Optimize the Geometry

With the initial geometries in hand, we can now move on to perform a proper geometry optimization.

To perform the calculations with `ORCA`, create a separate directory for each compound and copy the initial geometry (the `.xyz` file) into the corresponding directory.

The other thing that `ORCA` needs is an input file, where all the required information for the calculation is contained. These input files are plain text files, but with `.inp` instead of `.txt` as suffix.

The `ORCA` interpreter reads the input file line-by-line and differentiates three different 'environments':

1. Keywords (line starts with `!`. Used for general instructions.)
2. Blocks (line starts with `%[block]`, ends with `end`. Used for special instructions.)
3. Geometry (line starts with `*`, ends with another `*` if it spans multiple lines. Contains coordinates, charge, and multiplicity)

The interpreter does not care about capitalization or how many individual keyword lines you use, but try to keep your input files neatly formatted and readable.

---

#### Task 2
For each of your compounds, create an input file in their directories and perform the geometry optimizations.  
Use the following example input as guidance:

---

```
! BP86 def2-SV(P)
! OPT

* xyzFile 0 1 your_initial_geometry.xyz
```

For the geometry optimization example, the keywords `! BP86 def2-SV(P)` signal the method (`BP86` = a GGA DFT functional) and the basis set (`def2-SV(P)`). The keyword `OPT` signals that we want to do a geometry optimization.

(The `def2-SV(P)` basis set is essentially the efficient `def2-SVP` basis set with reduced polarization functions on hydrogen atoms. The basis set is thus very small, but it reduces computation time and is thus our basis set of choice here.)

To run a calculation, open a terminal and navigate into the calculation directory (which contains the `.xyz` file and the `.inp` file). Then type 

```
orca your_input_file.inp > your_output_file.out
```

to start the calculation. The terminal will become unresponsive for the duration of the calculation, which should take approx. 10 minutes.

Once the calculations are done, open the output file (`.out`) with a text editor and scroll to the very bottom. Make sure that it says: 

<center>

```
****ORCA TERMINATED NORMALLY****
```

</center>

But be careful: just because the calculation terminated normally does not mean that the geometry optimization converged! To check whether the optimization converged, also search the output file for:

<center>

```
  ***********************HURRAY********************
  ***        THE OPTIMIZATION HAS CONVERGED     ***
  *************************************************
```

</center>

---

#### Task 3
What files were created during the calculation? Find out what information they contain and their relative file sizes. What do the file extensions stand for?

---

### 3. Perform a Vibrational Analysis

The geometry optimization procedure is not foolproof. To be sure that the optimized geometry is stable, i.e. in a local minimum on the potential energy surface (PES), we need to perform a vibrational analysis at that point. To obtain the vibrational data, we need to perform a frequency calculation at the stationary point.

A geometry optimization is usually a cycle of:

1. Compute single point energy of given nuclear coordinates
2. Calculate first derivative of that single point on the PES to obtain displacement forces
3. Are the computed values below the convergence thresholds?
	1. If TRUE: signal convergence and end calculation
	2. If FALSE: displace nuclear coordinates accordingly and return to Step 1.

To ensure that we are in a *proper* local minimum (and not a saddle point), we need information from the second derivative (the *Hessian* matrix). Though somewhat time-consuming to compute, the Hessian is very valuable to us because it carries a lot of chemically relevant information.

The eigenvectors of the Hessian are the normal modes of the molecule in question, and the eigenvalues correspond to the frequencies of each mode.

For a local minimum then, all eigenvalues need to be real, positive numbers as negative force constants lead to imaginary frequencies, denoted in `ORCA` output files as negative frequencies.

---

#### Task 4
For one of your two optimized structures, perform a frequency calculation (approx. 20 minutes) and do a quick vibrational analysis (confirm a local minimum).

---

Use the same basis set and DFT functional as for the optimization, but make sure to change the keyword (frequency calculations are invoked with `! FREQ`) and load the correct, optimized `.xyz` file!

In the output of your calculation, find the `VIBRATIONAL FREQUENCIS` of your molecular normal modes and make sure that they are all real (i.e. non-imaginary).

III. Other Tools
----------------

<!-- In this exercise you have: -->

<!-- - installed `ORCA` and a 3D visualization software of your choice -->
<!-- - created the Cartesian coordinates from a Lewis structure -->
<!-- - performed a DFT geometry optimization of these coordinates and -->
<!-- - confirmed your optimized structure to be a true local minimum with a vibrational analysis -->

<!-- Almost all quantum chemical investigations start with such a workflow, since a correct ground state structure is the basis for all further examinations. -->

<!-- In the following exercises we will take learn about modern post-HF methods such as CI, CC, CASSCF.  -->

Although the main work is done by `ORCA`, there is a lot of overhead on performing these calculations.

`ORCA` has no GUI and must be operated through a terminal. Using a shell to interact with your computer is very powerful, as common tasks can be automated through scripts. On Linux/macOS there is the UNIX shell, which this exercise will use, but on Windows there is only the PowerShell (which uses a different syntax etc.). If you are using Windows, you can familiarize yourself with the PowerShell commands, or use an emulator for a UNIX shell, such as [cygwin](https://www.cygwin.com).

To navigate within a terminal might be cumbersome at first, but the benefits outweigh the learning curve (by a lot!). Here are some basic commands to get started:


| Command  | Function                |
|----------|-------------------------|
| cd       | Change directory        |
| pwd      | Print Working Directory |
| ls       | List files              |
| vi / vim | Text Editor             |
| mkdir    | Make Directory          |
| ...      |                         |


Explaining all the details of a command-line interface is beyond the scope of this too short introduction. The best way to learn anyhow is by *doing* things. We will touch more on this in the following exercises.

Here is a short example of using the shell to perform the tasks in this exercise (i.e. creating and navigating folders/directories):

```sh
pwd                   # `pwd` shows us where we are. `/` is the root directory.
> /Users/username     # as a user on your computer, the shell will start in your
                      # home directory, denoted `~`.

ls                    # List files and directories in our current directory.
> Applications
> Desktop
> Documents
> Downloads
> ...
``` 

Decide on where to run your calculations, perhaps in the `Documents` folder?

```sh
cd Documents          # move into `Documents`.

mkdir orca_calc       # create a new directory for the calculations.
cd orca_calc          # move into the new directory.

mkdir geosmin_opt     # create a new directory for a calculation.
cd geosmin_opt
```

Now we can create the input file with the `Vi` text editor.

```sh
pwd
> /Users/username/Documents/orca_calc/geosmin_opt

vi geosmin_opt.inp
```

This will now open the `Vi` interface. `Vi` is an extremely powerful text editor, even though it might not immediately look like one.

It is also very confusing to start out.

For now, suffice to say that there are two modes: Navigation and Insert mode. Opening a file starts you in Navigation mode, where all keys are hotkeys for navigation and text manipulation. The hotkey for entering the Insert mode (where you can *actually* type text) is `i`.

So with the input file opened, type `i` to enter Insert mode and type out whatever the input is. To save the file and exit, first exit the Insert mode by pressing `Esc`. Then type `:w` to save the file, and `:q` to exit the file. You can combine them as well and use `:wq` to save and exit. If you made some changes you don't want to save, use `:q!` to exit the file without saving.

```sh
ls
> geosmin_opt.inp

cat geosmin_opt.inp
> ! HF def2-SV(P)
> ! OPT
>
> * xyzFile 0 1 gesomin.xyz
```

The `cat` command shows you the content of a file directly in the terminal.

We have create the input file, but now we need the `geosmin.xyz` file! We have created the geometry in Avogadro and saved it to the `Desktop` (for example).

So now we want to copy the file from the `Desktop` to our `gesomin_opt` directory.

```sh
pwd
> /Users/username/Documents/orca_calc/geosmin_opt

ls ~/Desktop
> geosmin.xyz

cp ~/Desktop/geosmin.xyz ./geosmin.xyz
```

Remember that `~` is our home directory, i.e. `/Users/username`, and `./` denotes the directory that we are currently in.

After a final check, we are ready to start the calculation!

```sh
ls ./
> geosmin_opt.inp
> geosmin.xyz

orca geosmin_opt.inp > geosmin_opt.out
```

Here we are running `orca` with the settings specified in our `.inp` file. The calculation output is normally just printed to the terminal directly, but with the `>` character we can redirect this output, in this case to an `.out` file.

While the calculation is running, the terminal will become unresponsive. You can interrupt running processes with `Ctrl-c` (i.e. press `Ctrl` and `c` at the same time).

