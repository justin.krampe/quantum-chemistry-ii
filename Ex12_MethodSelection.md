Method Selection
================

Introduction
------------

In the last few weeks, we have explored the origin of various correlation and relativistic effects in post-Hartree-Fock methods. This week, we are going to study the implementation of these methods using two examples which were discussed recently in the literature. However, let us first recapture the implementation of dynamic and static (nondynamic) correlation effects in different approaches.

If we view electron correlation as an inadequacy of the single configuration Hartree-Fock-approximation, we can get a vague idea of these two effects. Therefore, on the one hand static correlation is the influence of other configurations that are low-lying in energy and that mix with the Hartree-Fock configuration. On the other hand, dynamic correlation arises from the $`r^{-1}_\mathrm{ij}`$ term in the Hamiltonian operator. This term is singular as $`r_\mathrm{ij}\rightarrow 0`$, and mathematical studies of the properties of exact wave functions show that the real wavefunction must contain cusps in $`r_\mathrm{ij}`$ to cancel this singularity. Still, the question arises how these two effects can be identified during actual calculations. Thus, we are going to analyze different diagnostic methods to detect static (nondynamic) and dynamic correlation in chemical systems in the following.

Let's revisit the smallest more electron problem: the $`\mathrm{H}_{2}`$ molecule. The simplest truncation which can be choosen to describe this kind of problem is to chose one $`N`$ electron basis function - a single configuration and a single determinant. In the case of a bonding $`\mathrm{H}_{2}`$ molecule such a single configuration with a single occupation determinant formed by the lowest $`\sigma`$- and $`\sigma`$*-orbitals in the $`\mathrm{H}_{2}`$ 
can be seen in the figure below:

![SC-SD](./source/images/ex_12/SC-SD.png)

Exercise
--------
<!--
- Write an ORCA input to investigate the CuH dissociation
  - with RHF and CASSCF (propose a CAS space)
  - perform the scan from 1.0 to 3.0 å w/ def2-SVP
  - plot both curves and compare both calculations at 1.5 and 2.5 å. Where do you expect which correlation (dynamic/static) to dominate?
    - analyze the CASSCF composition of root 0 at 1.5 and 2.5 å
    - give T1 diagnostic from CCSD(T)
  - how would you improve the calculation?
  - take a look at the T1 and T2 amplitude for the 'dissociation' of Cu2O2. Talk about that ?  
-->

In this example we want to investigate the dissociation of copper hydride, CuH, with a focus on selecting an appropriate method for our calculations.

Cu has a $`3d^94s^2`$ configuration, so the Cu<sup>+</sup> ion has a configuration of either $`3d^94s^1`$ or $`3d^{10}4s^0`$ (in principle $`3d^84s^2`$ is also possible).
Which configuration dominates depends on a multitude of factors, such as the difference in electron correlation energy between the two configurations and orbital energy levels.  
Both of these effects supposedly change with varying the distance between the Cu and the H atoms, so it makes sense to first do some explorative calculations to get a better understanding of the system.

1. Perform a dissociation scan on CuH with the RHF method and the def2-SVP basis set.

Since we expect electron correlation effects, it might also help to have a CASSCF calculation.

2. How would you construct an appropriate CAS space? (Which orbitals, and how many electrons?)
3. 




!["The T1 diagnostic (left) and the largest doubles contribution in the wave function (right) across the isomerization coordinate, calculated with the LPNO-CCSD method."](source/images/ex_12/cu2o2_t1_t2_diagnostic.png "The T1 diagnostic (left) and the largest doubles contribution in the wave function (right) across the isomerization coordinate, calculated with the LPNO-CCSD method.")
**Figure 1**
"The T1 diagnostic (left) and the largest doubles contribution in the wave function (right) across the isomerization coordinate, calculated with the LPNO-CCSD method."<sup>[1]</sup>

<!-- 
> In coupled cluster theory, the single excitation amplitudes essentially describe orbital relaxation, and hence, large single contributions are expected when the Hartree-Fock (HF) orbitals are poor.

> For genuine diradicals, the largest amplitude should approach a value of unity in which case the single reference approach as such  becomes invalid.

> A typical example of a system with large nondynamical correlation effects is a singlet biradical, where two determinants (at least) that differ by a double excitation are required for a formally correct description of the singlet configuration.

> Similarly, CC calculations predict that these isomers are each characterized by one very large T2 cluster amplitude corresponding to a double excitation with both electrons coming from the same occupied orbitals and being placed into the same virtual orbital.
-->


# References

[1] D. G. Liakos, F. Neese, "*Interplay of Correlation and Relativistic Effects in Correlated Calculations on Transition-Metal Complexes: The (Cu<sub>2</sub>O<sub>2</sub>)<sup>2+</sup> Core Revisited*", *J. Chem. Theory Comput.*, **2011**, *7*, 1511–1523. [DOI: 10.1021/ct1006949](https://dx.doi.org/10.1021/ct1006949)
