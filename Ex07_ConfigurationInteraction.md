Configuration Interaction and Coupled Cluster
=============================================

A short introduction
--------------------

Configuration Interaction (CI) and Coupled Cluster (CC) methods are closely related to each other, as both full CI and full CC yield the **exact** wave function and energy within the limits of the basis set used.

```math
|\Psi\rangle = \hat{C}|\Phi_0\rangle = \exp(\hat{T})|\Phi_0\rangle
```

The important benefit of the CC ansatz compared to the CI ansatz is that due to the exponential form, the CC ansatz includes contributions from *disconnected* excitations.

Take a look at the cluster decomposition of the CI operators:

```math
\hat{C}_1 = \hat{T}_1 \\
\hat{C}_2 = \hat{T}_2 + \frac{1}{2} \hat{T}_1^2  \\
\hat{C}_3 = \hat{T}_3 + \hat{T}_1\hat{T}_2 + \frac{1}{3!} \hat{T}_1^3  \\
\hat{C}_4 = \hat{T}_4 + \frac{1}{2} \hat{T}_2^2 + \hat{T}_1\hat{T}_3 + \frac{1}{4!} \hat{T}_1^4
```

The CC ansatz includes *all corrections of a given type* (i.e. S, D, T, Q, ..) *to infinite order*.

For example, take a look at the CCD wave function:

```math
|\Psi_{\text{CCD}}\rangle = \exp(\hat{T}_2)|\Phi_0\rangle = \left(\frac{1}{0!}\hat{T}_2^0 + \frac{1}{1!}\hat{T}_2^1 + \frac{1}{2!}\hat{T}_2^2 + \frac{1}{3!}\hat{T}_2^3 + \frac{1}{4!}\hat{T}_2^4 + ...\,\right)|\Phi_0\rangle
```

This wave function contains the contribution from *connected* two-electron excitations ($`\hat{T}_2^1`$), and also *disconnected* two-electron correlation effects, i.e. pair-pair ($`\hat{T}_2^2`$), pair-pair-pair ($`\hat{T}_2^3`$), ..., excitations.

To treat the disconnected pair-pair correlation in the CI formalism, we need to treat quadruple excitations as it is a formal four-electron process.

And exactly therein lies the major difference between the two methods. The fact that CC includes *products of excitations* makes it size-extensive, while the expansion into an infinite Taylor series means that the method is not variational. CI has the opposite problem, where the method is variational but *not* size-extensive.

(Also the CC ansatz is a lot more complex both in theory and implementation. The CC method is thus a relatively recent method for quantum chemistry programs.)

<!--
Compared to the instantaneous connected correlation of four electrons ($\hat{T}_4$), the *disconnected* correlation of two electron *pairs* (= also four electrons) ($\frac{1}{2}\hat{T}_2^2$) is likely to have a larger contribution to the overall correlation energy of a molecule.
-->

Cyclobutadiene
--------------

In the series of cyclic ethyne oligomeres, the trimer (cyclohexatriene) and tetramer (cyclooctatetraene) are well-known with quite established chemistry.
In this exercise, we will take a closer look at the "missing link": the dimer *cyclobutadiene*.

![Ethylene Oligomers](source/images/ex_7/acetylene_oligomeres.png)

The relative instability of cyclobutadiene is caused by two things:

1. With $`4n`$ (as opposed to $`4n+2`$) $`\pi`$-electrons, it is predicted to be *anti-aromatic*.
2. Bond angles of ca. $`90^\circ`$ (vs. ideally $`120^\circ`$) give rise to *ring strain*.

In this exercise, we will use high-level *ab-initio* methods to quantify the *exact* energy difference between two ethyne molecules and their dimer, the cyclobutadiene molecule.

00 - Lichtenberg II
-------------------

Accessing the Lichtenberg II cluster happens through an `ssh` (Secure SHell) tunnel via your terminal.

To start an `ssh` connection, type:

```sh
ssh <your_username>@lcluster14.hrz.tu-darmstadt.de
```

You will then be prompted to enter your password and press `Enter`.

(Note that while typing your password, no visible text will appear in the terminal. Also passwords are being phased out in favor of using cryptographic ssh keys, however their setup is more involved and thus not used for this course.)

If all goes well, you should now be connected to a `login node` on the cluster where you can execute the usual shell commands.

In your home directory `/home/kurse/kurse00063/<your_username>` you will set up your calculations and manage their files, while the *actual* calculation will be performed on a different node (= Computer) that is equipped with a large number of fast processors and storage.

There are a lot of large temporary files that are generated during an ORCA calculation, which are not needed once the calculation is done. Therefore the storage on the `compute nodes` is very large and fast, but it has **no** backup.

In contrast, your home directory on the `login node` has a comparatively small amount of storage space. But the home directories are regularly backed up, so a catastrophic data loss is unlikely.

To run a calculation, you need to bundle up your instructions and request the hardware you need. The cluster (more specifically: the SLURM workload manager) will then decide where and when to run your calculation, and after the calculation is done the output files can be copied back to your home directory.

01 - DFT Geometry Optimizations
-------------------------------

First we need these optimized geometries for our high-level calculations:

1. A single ethyne molecule (C2H2).
2. A cyclobutadiene molecule (C4H4).

You already know how to optimize a geometry with DFT, so to ensure that our results are comparable you don't have to do the optimizations yourself. Instead, you can find the optimized, ready-to-use structures on the cluster at `/home/kurse/kurs00063/material/dft_structures/`.

02 - CI and CC Single Point Energies
------------------------------------

In your home directory `/home/kurse/kurs00063/<your_username>/`, create four directories for the following calculations:

1. CISD cc-pVTZ/cc-pVQZ CBS Extrapolation
    - for a single ethyne molecule
    - for a cyclobutadiene molecule
2. CCSD cc-pVTZ/cc-pVQZ CBS Extrapolation
    - for a single ethyne molecule
    - for a cyclobutadiene molecule

For how to build an extrapolation input file, take a look back at [Exercise 4](https://git.rwth-aachen.de/ak-krewald/quantum-chemistry-ii/-/blob/main/Ex4_ElectronCorrelation.md).

Working on a high-performance cluser (HPC) requires us to do a little bit more: We need to specify the hardware that our calculation should run on.

Specifically, we need to specify the number of CPU cores, the memory of each CPU core, and the time limit of our calculation.

At the top of your input file, add the following lines:

```orca
%pal nprocs XXX end             # Specify between 1 and 8 CPU cores.
%maxcore XXX                    # Specify memory per core, up to 3800 MB.
#timelimit XXX                  # Specify time limit: 'short'=30min, 'medium'=24h, 'long'=3d.
```

For the extrapolation calculations use 8 CPU cores, 3800 MB per core, and decide on a time limit.

<!--
Example input:
```
%pal nprocs 8 end
%maxcore 3800
#timelimit long

! CCSD extrapolate(3/4,cc)

* xyzFile 0 1 ethyne.xyz
```
-->

To manage the computing resources of the HPC, the workload manager SLURM is used. This means that we write some instructions on how to perform the calculation and then submit our job to the SLURM queueing system.

Writing these instructions is cumbersome and repetitive work, so this is usually automated with a custom script. The shell script we will be using in this course is located at `/home/kurse/kurs00063/material/suborca/suborca.sh`.

If your calculation input file is set up (don't forget to copy the `.xyz` file to the folder as well!), navigate into the directory and call the submission script:

```sh
/home/kurse/kurs00063/material/suborca/suborca.sh my_input_file.inp
```

You can check the progress of your calculations with the command `squeue` and cancel jobs with `scancel <slurm_job_id>`.

03 - Size-Consistency
---------------------

With your calculations done, prepare reaction energy schemes for the CISD and the CCSD energies.

How do the two results differ?

By looking at the calculations, can you find where the difference between the two methods comes from?

How can you change your calculation setup to improve the CISD results?
