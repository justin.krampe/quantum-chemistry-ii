Relativity
==========

To treat relativistic effects, we need to move beyond the Schrödinger equation and use the more sophisticated Dirac equation.
The Dirac equation is four-dimensional (three spatial and one temporal dimension) and thus has four components ("the four-component Dirac equation"):

```math
\Psi_{\textrm{Dirac}} = 
\begin{pmatrix}
    \psi_1 \\
    \psi_2 \\
    \psi_3 \\
    \psi_4
\end{pmatrix}
```

Two of the four components are accounted for by spin ($`\alpha`$ and $`\beta`$) and the other two are interpreted as two different particles (the electron and "antielectron", i.e. the positron).
For pratical use, the four-component wavefunction is conventionally written as a two-component wavefunction:

```math
\Psi_{\text{Dirac}} = 
\begin{pmatrix}
    \psi_1 \\
    \psi_2 \\
    \psi_3 \\
    \psi_4
\end{pmatrix}
= 
\begin{pmatrix}
\Psi_L \\
\Psi_S
\end{pmatrix}
```

The two components are now referred to as the *large* component ($`\Psi_L`$) and the _small_ component ($`\Psi_S`$).
For electrons, the large component reduces to the solutions of the Schrödinger equation (its two components corresponding to $`\alpha`$ and $`\beta`$ spin) when the speed of light approaches infinity ($`c \rightarrow \infty`$), while the small component disappears, giving rise to the names.
In contrast, the large component disappears for the negative energy solutions corresponding to the positrons.

The large and small components can now be decoupled by a unitary transformation so that the positive (electronic) energy states are only described by the transformed large component $`'\Psi_L`$ and the transformed small component can thus be neglected.

In the **Douglas-Kroll-Hess (DKH)** approach, these unitary transformations are performed iteratively to decouple the large and small components to higher and higher orders.
Carried out to infinite order, the DKH approach is equivalent to solving the full four-component Dirac equation within the two-component framework.

The resulting equations still mix both spin components, so the usuall approach is to perform an additional decomposition into a spin-free ("scalar relativistic") and a spin-dependent ("spin-orbit coupling") part.
The spin-free part is straightforward to solve and the spin-dependent part can then be added perturbatively or via other means afterwards.

An important point to take into consideration now is that within our quantum chemistry codes, the operators for molecular properties are derived for the non-relativistic Schrödinger equation.
To compute the properties for our unitarily-transformed DKH wavefunction, the corresponding operators need to be transformed as well.
This is not a physical effect, but a result of us mixing a relativistic wavefunction with non-relativistic operators.
These effects are called **picture change effects** and can be remedied by performing the unitary transformation for the operators in question.
To do this in `ORCA`:

```orca
%rel
    picturechange True
end
```

It is recommended to turn this on for basically *all* relativistc calculations!

Another quirk of the software that we need to consider is that we usually (i.e. in non-relativistc calculations) assume the nucleus of an atom to have no spatial extent.
While reasonable for non-relativistic calculations, taking the spatial extent of the nucleus into account can be necessary for certain relativistic computations, for example in investigations of core excited states (X-ray absorption spectroscopy).
This can be toggled in `ORCA` with:

```orca
%rel
    finiteNuc True
end
```

The phenomenological effects of relativistic on quantum chemical calculations can be summed up as:

- Core-electrons of heavier elements move at an appreciable chunk of the speed of light, gaining relativistic mass as a result.
This leads to a contraction (= energetic stabilization) of the *s*-orbitals, while the *d*- and *f*-orbitals expand (= energetic destabilization) as a result of the increasingly shielded nuclear charge. (*p*-orbitals remain relatively (haha) unchanged, due to competing scalar and spin-orbit relativistic effects.)
- Spin-orbit coupling means that our separation of spin $`\vec{s}`$ and orbital $`\vec{l}`$ angular momentum is no longer sound, instead we now consider only the *total* angular momentum $`\vec{j} = \vec{s} + \vec{l}`$.
- Relativistic orbitals do not have nodes, as the small component of the wavefunction is finite even when the large component is zero.

Since a large part of the relativistic effects are concentrated on the core orbitals, which don't change much during regular chemical reactions, we (immensely) benefit from large error cancellation for relative energies (the *absolute* energy difference for relativistic and non-relativistic H<sub>2</sub>O is already +145 kJ/mol!).

Also the relativistic effects scale with increasing attraction between the electrons and the nucleus, i.e. with the atomic number $`Z`$.
Relativistic effects are *usually* negligible for the first four rows of the periodic table (up to $`Z = 36`$, Krypton).
However remember that for effects involving electron spin (i.e. spin-orbit coupling) that are purely relativistic in origin, the relativistic correction is everything!

Another practically relevant observation is that the relativistic corrections change our orbitals.
Relativistic calculations **require** the use of special-made basis sets!
Fortunately, suitable relativistically (re-)contracted basis sets are implemented directly in `ORCA`:

```orca
for DKH calculations (defined up to Kr):
DKH-def2-(SVP, SV(P), TZVP, TZVPP, QZVPP)

for correlated DKH calculations:
cc-pVnZ-DK

for ZORA calculations (defined up to Kr):
ZORA-def2-(SVP, SV(P), TZVP, TZVPP, QZVPP)

for elements beyond Kr:
SARC-(DKH,ZORA)-(TZVP, TZVPP)
```

If the problem at hand is almost exclusively dominated by scalar relativistic effects (i.e. the effects are concentrated in the core orbitals), one can include these effects *implicitly* with a suitable pseudopotential.
As noted before, the core orbitals remain relatively unchanged during chemical transformations, so if the spin-orbit coupling can be neglected, the only relativisitc effect that remains is the reduced effective nuclear charge.
Some basis sets (e.g. the Karlsruhe def2 family) thus have the option to use "effective core potentials" (ECPs) that use precomputed nuclear charges to treat the relativistic effects implicitly and, most importantly, cheaply in an otherwise non-relativistic calculation.
(Don't use ECPs when invoking a relativistic correction, use one of the appropriate basis sets from above.)

Example
-------

Let's look at an example from exercise 10 last week, the thallium hydride (TlH) bond length.
For this investigation, we will need four calculations:

1. non-relativistic with ECPs
2. relativistic with ZORA
3. relativistic with DKH to first order
4. relativistic with DKH to second order

The non-relativistic geometry optimization is straightforward, as the ECPs are invoked automatically:

```orca
! B3LYP def2-TZVP AutoAux
! Opt

* xyz 0 1
Tl 0 0 0
H  0 0 1
*
```

For the relativistic calculations, we need to first decide on an appropriate basis set.
The atomic number of thallium is $`Z=81`$, for which the `ZORA-def2-TZVP` basis set is not defined.
We thus choose the segmented all-electron relativistic basis set `SARC-(ZORA/DKH)-TZVP`.
One caveat: this basis set is in turn not defined for hydrogen, so we have to set its basis set separately:

```orca
! B3LYP ZORA SARC-ZORA-TZVP AutoAux
! Opt

%basis
    newGTO H "def2-TZVP" end
end

* xyz 0 1
Tl 0 0 0
H  0 0 1
*
```

The same input as above can be used for the two DKH calculations by exchanging the keyword (`DKH1` for first-order DKH and `DKH` or `DKH2` for the second-order DKH) and using the `SARC-DKH-TZVP` basis set.

(Each calculation should take ca. 10 minutes. If your computer has multiple CPU cores (which it should), you can run the calculations simultaneously by launching each calculation in a separate terminal window.)

Compare the obtained TlH bond lengths.
Can you explain their relative performance?
What's up with the final single point energies of the calculations?
How could we improve our calculation method to get an even better result?

(Note: `ORCA` automatically invokes a 'one-center' approximation for relativistic geometry optimziations, so the obtained energies are different from single point calculations and they should not be mixed!)
