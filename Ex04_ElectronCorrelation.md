Electon Correlation
===================


I. Shortcomings of Hartree-Fock
-------------------------------

To investigate the effects of electron correlation, we will take a look at the dissociation of $`H_2`$.

Combining the two atomic orbitals of two hydrogen atoms in a LCAO (Linear Combination of Atomic Orbitals) ansatz yields the two molecular orbitals (bonding: $`\sigma^b`$ antibonding: $`\sigma^{*}`$) of the dihydrogen molecule.

![Molecular Orbital Diagram of Dihydrogen](source/images/ex_4/dihydrogen_mo.png "Molecular orbital diagram of dihydrogen via the LCAO ansatz.")

By increasing the bond length, we expect an increase in energy that converges in the limit of infinite separation of the two hydrogen atoms.

![Energy Diagram of the Dihydrogen Dissociation](source/images/ex_4/dihydrogen_dissociation.png "Energy diagram of the dihydrogen dissociation")

We will replicate this curve by calculating single point energies at various interatomic distances, using two flavours of the Hartree-Fock approach: *restricted* and *unrestricted* HF.

---

#### Task 1

Which of these two approaches do you think is more suitable for investigating this bond dissociation? Draw a schematic energy diagram (like the one above) with two curves, one for the RHF and one for the UHF method. Also add the schematic orbital occupation diagrams at the equilibrium distance and at infinite separation for the two methods.

---

To perform a scan on the potential energy surface (PES) in `ORCA`, we use the `%GEOM` block:

```
%GEOM
	Scan B 0 1 = 0.4, 8.0, 39 end
end
```

We tell the program to `Scan` the `B`ond between atoms `0` and `1`, from `0.4` to `8.0` Å in `39` steps.

The rest of the input is then simple, we need to specify the method and basis set as well as some information on the molecule. Note that for `ORCA` to actually parse the `%GEOM` block, we need to use the `! OPT` keyword. Also, since the molecule is so simple we can write the coordinates directly into the input and don't need a separate `.xyz` file.

```
! RHF def2-SVP
! OPT

%GEOM
	Scan B 0 1 = 0.4, 8.0, 39 end
end

* xyz 0 1
H 0 0 0
H 0 0 1
*
```

For the UHF calculation we need to additionally modify the `%SCF` block to perform a broken-symmetry calculation:

```
! UHF def2-SVP 
! OPT
%GEOM 
Scan B 0 1 = 0.4, 8.0, 39 end 
end

%SCF
	BrokenSym 1,1		# one spin-up and one spin-down electron = open-shell singlet
end

* xyz 0 1
H 0 0 0
H 0 0 1
*
```

---

#### Task 2
Perform the RHF and BS-UHF dihydrogen bond dissociation scans and extract the SCF energies. Create a plot with the RHF, BS-UHF, and CISD (Configuration Interaction Singles and Doubles) data below.

You will learn more about the configuration interaction method at a later point, for now (and for dihydrogen specifically) think of the CISD data as full CI data, i.e. as numerically exact (within the limits of the chosen basis set).

- Mark the correlation energy.
- Mark the bond dissociation energy.
- Discuss the BS-UHF curve.
	+ Why does it follow the RHF curve near the bound region, but the CISD curve at long distances?
- To what energy value does each curve converge? Why does the RHF curve converge much slower?

---

<center>

#### CISD/def2-SVP Dihydrogen Bond Dissociation Scan

|   Bond Length [Å]  |   CISD [E<sub>h</sub>]  |
|--------------------|-------------------------|
|   0.4              |   -0.9447               |
|   0.6              |   -1.1396               |
|   0.8              |   -1.1626               |
|   1.0              |   -1.1396               |
|   1.2              |   -1.1063               |
|   1.4              |   -1.0747               |
|   1.6              |   -1.0490               |
|   1.8              |   -1.0301               |
|   2.0              |   -1.0174               |
|   2.2              |   -1.0094               |
|   2.4              |   -1.0047               |
|   2.6              |   -1.0019               |
|   2.8              |   -1.0004               |
|   3.0              |   -0.9995               |
|   3.2              |   -0.9991               |
|   3.4              |   -0.9988               |
|   3.6              |   -0.9987               |
|   3.8              |   -0.9986               |
|   4.0              |   -0.9986               |
|   4.2              |   -0.9986               |
|   4.4              |   -0.9986               |
|   4.6              |   -0.9986               |
|   4.8              |   -0.9986               |
|   5.0              |   -0.9986               |
|   5.2              |   -0.9986               |
|   5.4              |   -0.9986               |
|   5.6              |   -0.9986               |
|   5.8              |   -0.9986               |
|   6.0              |   -0.9986               |
|   6.2              |   -0.9986               |
|   6.4              |   -0.9986               |
|   6.6              |   -0.9986               |
|   6.8              |   -0.9986               |
|   7.0              |   -0.9986               |
|   7.2              |   -0.9986               |
|   7.4              |   -0.9986               |
|   7.6              |   -0.9986               |
|   7.8              |   -0.9986               |
|   8.0              |   -0.9986               |

</center>


II. The Complete Basis Set Limit
--------------------------------
<!-- 
cc vs def2
Extrapolation to the CBS
-->

Treating electron correlation effects thus requires a suitable method, and even then accurate correlation energies require large basis sets, ideally up to the complete basis set (CBS) limit. 

---

#### Task 3

Calculate and plot the HF and CISD final single point energies of dihydrogen (r = 0.8 Å) for the three cardinal numbers 2, 3, and 4 (i.e. double-$`\zeta`$, triple-$`\zeta`$, quadruple-$`\zeta`$, ..) with the `def2` and `cc` basis sets (see example input below). Explain the observed differences in:

- total energy
- convergence behaviour between HF and CISD
- convergence behaviour between `def2` and `cc`

---

Example input for performing successive single point calculations:

```
! CISD cc-pVDZ

* xyz 0 1
H 0 0 0
H 0 0 0.8
*

$new_job

! CISD cc-pVTZ

* xyz 0 1
H 0 0 0
H 0 0 0.8
*

$new_job

! CISD cc-pVQZ

* xyz 0 1
H 0 0 0
H 0 0 0.8
*
```

Manually approaching the CBS limit is tedious, and so as a computationally efficient method, the energies in the CBS limit can be extrapolated from the results of two calculations with basis sets of reasonable size.

The total energy in a CBS $`E_{\text{tot}}^{\infty}`$ can be separated into the SCF and correlation energy contributions:

```math
E_{\text{tot}}^{\infty} = E_{\text{SCF}}^{\infty} + E_{\text{Corr}}^{\infty}
```

The SCF energy $`E_{\text{SCF}}^{X}`$ with a basis set of cardinal number $`X`$ converges according to:

```math
E_{\text{SCF}}^{X} = E_{\text{SCF}}^{\infty} + A \exp{\left(-\alpha\sqrt{X}\right)}
```

$`A`$ is to be determined and $`\alpha`$ is a basis set specific constant.

The correlation energy $`E_{\text{Corr}}^{X}`$ converges differently and requires two separate basis sets:

```math
E_{\text{Corr}}^{\infty} = \frac{X^{\beta}E_{\text{Corr}}^{X} - Y^{\beta}E_{\text{Corr}}^{Y}}{X^{\beta} - Y^{\beta}}
```

The coefficients $`\alpha`$ and $`\beta`$ are dependent on the basis sets used and optimized values are automatically chosen in `ORCA`.

An example input for a two-point CBS extrapolation using `cc-pVDZ` and `cc-pVTZ`:

```
! HF Extrapolate(2/3,cc)

* xyz 0 1
H 0 0 0
H 0 0 0.8
*
```

---

#### Task 4

Perform the HF and CISD CBS extrapolations for both the `def2` and `cc` basis sets with the `2/3` and `3/4` combinations (8 calculations total). Remember that you can write them in one input file with `$new_job`.
Shortly discuss your obtained CBS extrapolation results with regards to your results from [Task 3](#task-3).

___
